 import java.util.Scanner;
 
public class patternC {
	public static void main(String [] args) {
		Scanner myScanner = new Scanner(System.in);

			System.out.println("Please enter an integer between 1 and 10."); // asks user for input
		int input = myScanner.nextInt();
			do { // creates infinite loop that will only be broken if user enters right numbers
				if (input < 1 || input > 10) { // ensures input is within proper range, loops if not
				System.out.println("Please enter an integer that is within the bounds of 1 and 10.");
				input = myScanner.nextInt();
			}
				} 
			while (input < 1 || input > 10);
			
		for (int numRows = 1; numRows <= input; numRows++) { // loops for the number of rows
			for (int numLine = numRows; numLine >= 1; numLine--) { // loops for the numbers in each line
				System.out.print(numLine); 
			}
		System.out.println();	// puts a space between lines
		}
	}
	}


