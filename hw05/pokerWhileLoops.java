/*
Larrisa Miller
10/17/2018
CSE 002 
pokerWhileLoops: generates however many hands of cards a user wants and calculates statistics based on those cards
*/

import java.util.Scanner;

public class pokerWhileLoops {
	public static void main (String [] args){
		Scanner myScanner = new Scanner(System.in);

		// acquiring value for number of hands
		System.out.println("How many times would you like to generate hands?");
		boolean numHandsBool = myScanner.hasNextInt();// scanner tests if what the user enters is an int
		while (numHandsBool != true){ // if the user enters something other than an int, the bool is evaulated as false and this loop is taken
			System.out.println("Please enter an integer, such as 100 or 1000."); 
			myScanner.next(); // removes incorrect input to prevent infinite loop
			numHandsBool = myScanner.hasNextInt(); // open for new input
		}
		int numHands = myScanner.nextInt();

		int fourOfAKind = 0; // declare ints and booleans to be used later
		boolean fourBool;
		int threeOfAKind = 0;
		boolean threeBool;
		int twoPair = 0;
		int twoPairNum; 
		int onePair = 0;
		int onePairNum; 

		int handsGen = 0;		    
		while (handsGen <= numHands) {
			int temp; // ensuring that each card is assigned a different number from the rest
			int card1 = (int)(Math.random()*(52+1))+1; 
			do {
				temp = (int)(Math.random()*(52+1))+1; 
			} while (card1 == temp); // while the next card is = to card 1, continue looping through random numbers until the two cards are different
			int card2 = temp;
			do {
				temp = (int)(Math.random()*(52+1))+1;
			} while (card1 == temp || card2 == temp);
			int card3 = temp;
			do {
				temp = (int)(Math.random()*(52+1))+1;
			} while (card1 == temp || card2 == temp || card3 == temp);
			int card4 = temp;
			do {
				temp = (int)(Math.random()*(52+1))+1;
			} while (card1 == temp || card2 == temp || card3 == temp || card4 == temp);
			int card5 = temp;
			temp = 0;
			handsGen++; // loops until it creates the numbers of hands that user specified

			fourBool = false;
			threeBool = false;
			twoPairNum = -1;
			onePairNum = -1;

			int cardNum = 0;
			while (cardNum < 5) { // iterate through each card in a hand
				int compareCard = 0;
				if (cardNum == 0) { // 
					compareCard = card1;
				}
				if (cardNum == 1) {
					compareCard = card2;
				}
				if (cardNum == 2) {
					compareCard = card3;
				}
				if (cardNum == 3) {
					compareCard = card4;
				}
				if (cardNum == 4) {
					compareCard = card5;
				}

				compareCard %= 13; // modulus breaks down all of the card numbers into 0-12
				cardNum++;

				int cardCount = 0; // telling which card we are comparing against
				int sameNumCounter = 0; // count number of cards w/ same number
				while (cardCount< 5) {
					if (cardCount == 0 && card1 % 13 == compareCard) { // counts number of time card has occurred
						sameNumCounter++;
					}
					if (cardCount == 1 && card2 % 13 == compareCard) {
						sameNumCounter++;
					}
					if (cardCount == 2 && card3 % 13 == compareCard) {
						sameNumCounter++;
					}
					if (cardCount == 3 && card4 % 13 == compareCard) {
						sameNumCounter++;
					}
					if (cardCount == 4 && card5 % 13 == compareCard) {
						sameNumCounter++;
					}
					
					cardCount++;
					
					if (sameNumCounter == 4 && !fourBool) {
						fourBool = true;
						fourOfAKind++;
					}
					if (sameNumCounter == 3 && !threeBool) {
						threeBool = true;
						threeOfAKind++;
					}
					if (sameNumCounter == 2 && onePairNum == -1) {
						onePairNum = compareCard;	
						onePair++;
					}
					if (sameNumCounter == 2 && twoPairNum == -1 && onePairNum != compareCard) {
						twoPairNum = compareCard;
						twoPair++;
						onePair--; // if there are two pairs, then 1 pair can't exist because it requires 3 different cards
					}
				}
			}

		}
		System.out.println("The number of loops: " + numHands);
		System.out.printf("The probability of Four-of-a-kind: %.3f \n" , (double) fourOfAKind/numHands);
		System.out.printf("The probability of Three-of-a-kind: %.3f \n", (double) threeOfAKind/numHands);
		System.out.printf("The probability of Two-pair: %.3f \n", (double) twoPair/numHands);
		System.out.printf("The probability of One-pair: %.3f \n", (double) onePair/ numHands);
	}
}


