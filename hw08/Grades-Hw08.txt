Grade: 90

Compiles    				                  20 pts -- 20
Comments 				                      10 pts -- 10
Method shuffle(list)			            20 pts -- 20
Method getHand(list,index, numCards)  20 pts -- 20
Method printArray(list)               20 pts -- 20
Checks numCards                       10 pts -- 10

Deductions:
-10: Late submission (1 day)

EGC