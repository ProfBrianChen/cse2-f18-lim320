/* Larrisa Miller
  9/7/2018
  CSE 02
  Cyclometer program purpose: bicycle cyclometer that measures speed, distance, etc. 
  This program will print the number of minutes for each trip, counts for each trip, distance of each trip in miles, and distance of two trips combined.
  */

public class Cyclometer {
  public static void main (String [] args){
    
    // input data variables used in program
    int secsTrip1=480; // # of seconds in trip 1
    int secsTrip2=3220; // # of seconds in trip 2
    int countsTrip1=1561; // # of counts in trip 1
    int countsTrip2=9037; // # of counts in trip 2 
    
    // variables for constants and storing distances
    double wheelDiameter=27.0; // constant for wheel diameter
    double PI=3.14159; // value of pi
    double feetPerMile=5280; // constant value of feet per mile 
    double inchesPerFoot=12; // constant value of inches per foot 
    double secondsPerMinute=60; // constant value of seconds per minute 
    double distanceTrip1; // reserves space for the distance of trip 1 
    double distanceTrip2; // reserves space for the distance of trip 2
    double totalDistance; // reserves space for distance of the total trip 
    
    // print out number of minutes trips took and counts 
    System.out.println("Trip 1 took "+ secsTrip1/secondsPerMinute +" minutes and had "+countsTrip1+ " counts.");
    System.out.println("Trip 2 took "+ secsTrip2/secondsPerMinute +" minutes and had "+countsTrip2+ " counts.");
    
    // calculate the values of distance travelled
    distanceTrip1=countsTrip1*wheelDiameter*PI; //gives distance in inches 
    distanceTrip1/= inchesPerFoot*feetPerMile; // gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // gives distance in miles 
    totalDistance=distanceTrip1+distanceTrip2; // adds both distances together to get final distance 
    
    //print out distances as output data
   System.out.println("Trip 1 was "+distanceTrip1+" miles."); 
   System.out.println("Trip 2 was "+distanceTrip2+" miles.");
   System.out.println("The total distance was "+totalDistance+" miles.");
    
  }
}