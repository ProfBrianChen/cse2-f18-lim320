/* Larrisa Miller
  9/14/2018
  CSE 02
  Check: this program allows the user to split acheck evenly with other people.
  */

import java.util.Scanner;

public class Check{ // main method required for every Java program 
  public static void main(String [] args){
   Scanner myScanner = new Scanner(System.in); // creates scanner to allow user input for various variables
   
    // prompting user to input check values
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // asks user for original cost of check
    double checkCost = myScanner.nextDouble(); // creates variable to hold original check cost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // asks user for how much they want to tip
    double tipPercent = myScanner.nextDouble(); // creates variable to hold desired tip percentage
    tipPercent /= 100; //we want to convert the percentage into a decimal value 
    System.out.print("Enter the number of people who went out to dinner: "); // asks user for number of people at dinner
    int numPeople = myScanner.nextInt(); // creates variable to hold number of people
    
    //outputting the amount that each member of the group needs to spend on the check 
    double totalCost;
    double costPerPerson;
    int dollars; //whole dollar amount of costPerPerson
    int dimes, pennies; // for storing digits to the right of the decimal point for the cost$
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; 
    dollars = (int) costPerPerson; // get the whole amount, dropping decimal fraction 
    dimes = (int) (costPerPerson * 10) % 10; // get dimes amount 
    pennies = (int) (costPerPerson * 100) % 10; // get pennies amount
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); 
    
  } // end of main method
} // end of class