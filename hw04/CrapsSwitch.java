/* Larrisa Miller
   9/23/2018
   CSE 002
   CrapsSwitch: generates slang for 2 die according to the game craps using only switch statements
*/
import java.util.Scanner;

public class CrapsSwitch{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    int diceOne = -1; // declaring variables that can be used in all loops; these store the two dice values that will be used for slang.
    int diceTwo = -1; // initialized as -1 to ensure that this value is properly changed within remaining code.
    
    // establishes how user wants to play the game
    System.out.println("If you would like to randomly cast dice, enter 0. If you would like to state the two dice that you want to evaluate, enter 1.");
    int userInput = myScanner.nextInt(); 
    
    switch (userInput) {
      case 0: // if the user wants both dice to be random, this puts a random number from 1-6 in each dice value
        diceOne = (int)(Math.random()*(6+1)) + 1;
        diceTwo = (int)(Math.random()*(6+1)) + 1;
        break;
      case 1: // if the user wants to input their own values
        System.out.println("Please enter the number (1-6) of your first dice.");
            diceOne = myScanner.nextInt(); // inputs value into dice 1
          switch (diceOne) { // switch statements assign numerical values to dice one 
            case 1: // if user enters 1, diceOne is assigned as 1
              diceOne = 1;
              break;
            case 2:
              diceOne = 2;
              break;
            case 3:
              diceOne = 3;
              break;
            case 4:
              diceOne = 4;
              break;
            case 5:
              diceOne = 5;
              break;
            case 6:
              diceOne = 6;
              break;
            default:
              System.out.println("Invalid input. Please start the program again and use values between 1 and 6.");
              return; // if user enters a value < 1 && > 6 the program quits
          }
        
        System.out.println("Please enter the number (1-6) of your second dice.");
            diceTwo = myScanner.nextInt(); // inputs value into dice 2
        switch (diceTwo){ // switch statements assign numerical values to dice 2
            case 1: //if user enters 1, diceTwo is assigned as 1
              diceTwo = 1;
              break;
            case 2:
              diceTwo = 2;
              break;
            case 3:
              diceTwo = 3;
              break;
            case 4:
              diceTwo = 4;
              break;
            case 5:
              diceTwo = 5;
              break;
            case 6:
              diceTwo = 6;
              break;
            default:
              System.out.println("Invalid input. Please start the program again and use values between 1 and 6.");
              return; // if user enters a value < 1 && > 6 the program quits
          }
        break;
      default:
        System.out.println("Please start the program over and select 0 or 1.");
        return; // if user enters a value that isn't 1 or 0 the program quits
        }
       
// assigns the slang names based on what numbers are generated above
    switch (diceOne){ //outer switch statement based on what dice 1 has been assigned
      case 1:
        switch (diceTwo){ 
          case 1: // if diceOne is 1 and diceTwo is 1, system prints out "snake eyes"
            System.out.println("Snake Eyes");
            break;
          case 2:
            System.out.println("Ace Deuce");
            break;
          case 3:
            System.out.println("Easy Four");
            break;
          case 4:
            System.out.println("Fever Five");
            break;
          case 5:
            System.out.println("Easy Six");
            break;
          case 6:
            System.out.println("Seven Out");
            break;
        }
        break; // ensures that the program quits after the first value for diceOne and diceTwo are declared
      case 2:
        switch (diceTwo){
          case 1: 
            System.out.println("Ace Deuce");
            break;
          case 2: 
            System.out.println("Hard Four");
            break;
          case 3:
            System.out.println("Fever Five");
            break;
          case 4:
            System.out.println("Easy Six");
            break;
          case 5:
            System.out.println("Seven Out");
            break;
          case 6: 
            System.out.println("Easy Eight");
            break;
        }
        break;
      case 3:
        switch (diceTwo){
          case 1: 
            System.out.println("Easy Four");
            break;
          case 2:
            System.out.println("Fever Five");
            break;
          case 3:
            System.out.println("Hard Six");
            break;
          case 4:
            System.out.println("Seven Out");
            break;
          case 5:
            System.out.println("Easy Eight");
            break;
          case 6: 
            System.out.println("Nine");
            break;
        }
        break;
      case 4:
        switch (diceTwo){
          case 1: 
            System.out.println("Fever Five");
            break;
          case 2:
            System.out.println("Easy Six");
            break;
          case 3:
            System.out.println("Seven Out");
            break;
          case 4:
            System.out.println("Hard Eight");
            break;
          case 5:
            System.out.println("Nine");
            break;
          case 6:
            System.out.println("Easy Ten");
            break;
        }
        break;
      case 5:
        switch (diceTwo){
          case 1:
            System.out.println("Easy Six");
            break;
          case 2:
            System.out.println("Seven Out");
            break;
          case 3:
            System.out.println("Easy Eight");
            break;
          case 4:
            System.out.println("Nine");
            break;
          case 5:
            System.out.println("Hard Ten");
            break;
          case 6:
            System.out.println("Yo-leven");
            break;
        }
        break;
      case 6:
        switch (diceTwo){
          case 1:
            System.out.println("Seven Out");
            break;
          case 2:
            System.out.println("Easy Eight");
          case 3:
            System.out.println("Nine");
            break;
          case 4:
            System.out.println("Easy Ten");
            break;
          case 5:
            System.out.println("Yo-leven");
            break;
          case 6:
            System.out.println("Boxcars");
            break;
        }
    }
  }
}
        
      
        
   
    
  