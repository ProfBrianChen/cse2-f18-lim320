/* Larrisa Miller
   9/9/2018
   CSE 002
   Arithmetic program purpose:compute the cost of items bought, including PA sales tax
*/

public class Arithmetic {
  public static void main (String [] args){
    
    // input variables
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    int numShirts = 2; // number of sweatshirts
    double shirtPrice = 24.99; // cost per sweatshirt 
    int numBelts = 1; // number of belts
    double beltCost = 33.99; // cost per belt
    double paSalesTax = 0.06; // PA sales tax 
    
    // variables for costs
    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts; // total cost of shirts
    double totalCostofBelts; // total cost of belts
    double salesTaxOnPants; // sales tax on pants 
    double salesTaxOnShirts; // sales tax on shirts
    double salesTaxOnBelts; // sales tax on belts
    double totalCostWithoutTax; // total cost without tax
    double totalSalesTax; // total sales tax
    double totalPaid; // total amount paid
    
    // calculations for pants 
    totalCostOfPants = numPants * pantsPrice; // multiply number of pants by the price of pants to find total cost of pants
    totalCostOfPants *= 100; // multiply cost by 100 so entire price is before decimal place
    totalCostOfPants = (int) totalCostOfPants / 100.0; // convert to int & divide to move cost to only 2 decimal points
    
    salesTaxOnPants = (totalCostOfPants * paSalesTax); // multiply total cost of pants by the PA sales tax
    salesTaxOnPants *= 100; // multiply cost by 100 so entire price is before decimal place
    salesTaxOnPants = (int) salesTaxOnPants / 100.0; // convert to int & divide to move cost to only 2 decimal points 
    
    System.out.println("The cost of pants is $" + totalCostOfPants + " and the sales tax paid for pants is $" + salesTaxOnPants + "."); // print the cost of pants and sales tax 
    
   // calculations for shirts: same logic as shirts
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfShirts *= 100;
    totalCostOfShirts = (int) totalCostOfShirts / 100.0;
    
    salesTaxOnShirts = totalCostOfShirts * paSalesTax;
    salesTaxOnShirts *= 100;
    salesTaxOnShirts = (int) salesTaxOnShirts / 100.0;
    
    System.out.println("The cost of shirts is $" + totalCostOfShirts + " and the sales tax paid for shirts is $" + salesTaxOnShirts + ".");
    
    // calculations for belts: same logic as pants
    totalCostofBelts = numBelts * beltCost;
    totalCostofBelts *= 100;
    totalCostofBelts = (int) totalCostofBelts / 100.0;
    
    salesTaxOnBelts = totalCostofBelts * paSalesTax;
    salesTaxOnBelts *= 100;
    salesTaxOnBelts = (int) salesTaxOnBelts / 100.0;
    
    System.out.println("The cost of belts is $" + totalCostofBelts + " and the sales tax paid for belts is $" + salesTaxOnBelts + ".");
    
    // total costs
    totalCostWithoutTax = totalCostOfPants + totalCostOfShirts + totalCostofBelts; // add individual costs without sales tax together to get total cost without sales tax
    totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts; // add individual sales tax together to get total sales tax
    totalPaid = totalCostWithoutTax + totalSalesTax; // add together total cost without tax & total sales tax to get total amount paid
    
    System.out.println("The total cost of the purchases before tax is $" + totalCostWithoutTax + " and the total sales tax paid is $" + totalSalesTax + ".");
    System.out.println("The total amount paid is $" + totalPaid + "."); // print out totals
    
  }
}
