	import java.util.Scanner;
	public class removeElements{
	  public static void main(String [] arg){
		Scanner scan=new Scanner(System.in);
	int num[]=new int[10];
	int newArray1[];
	int newArray2[];
	int index,target;
		String answer="";
		do{
	  	System.out.print("Random input 10 ints [0-9]");
	  	num = randomInput();
	  	String out = "The original array is:";
	  	out += listArray(num);
	  	System.out.println(out);
	 
	  	System.out.print("Enter the index ");
	  	index = scan.nextInt();
	  	newArray1 = delete(num,index);
	  	String out1="The output array is ";
	  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out1);
	 
	      System.out.print("Enter the target value ");
	  	target = scan.nextInt();
	  	newArray2 = remove(num,target);
	  	String out2="The output array is ";
	  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out2);
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
	  	answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	  }
	 
	  public static String listArray(int num[]){
		String out="{";
		for(int j=0;j<num.length;j++){
	  	if(j>0){
	    	out+=", ";
	  	}
	  	out+=num[j];
		}
		out+="} ";
		return out;
	  }

public static int[] randomInput(){ 
		
		int num[] = new int[10]; //Creates an array of 10 integers
		
		for (int i=0; i <10; i++){ // fills the array with random numbers 0 - 9
			num[i] = (int)(Math.random() *10);		 
		}
		return num;
	}


	public static int[] delete(int list[], int pos){
		
		if (pos > 0 && pos < 9){
			int newArray[] = new int[9]; //Creates an array that has one member fewer than list
			
			int i=0;
			int j=0;
			
			while (i< newArray.length){  //Loops until every value has been stored except for value in pos position
				if (i==pos){
					j++; //Removes element in pos position
				}
				newArray[i] = list[j]; //Assigns values stored in list in the new array
				i++;
				j++;	
			}
			System.out.println("Index " + pos + " element is removed.");
			return newArray; // returns new array with pos element removed
		}
		else{ //If index given is invalid, return list.
			System.out.println("The index is not valid.");
			return list.clone();
		}
	}

	public static int[] remove(int list[], int target){
		
		int counter = 0;
		
		for(int i = 0; i < list.length; i++){
			if (target == list[i]) {
				counter++;
			}//Skips every element on the list that is equal to target
		}

		int Array2[] = new int[list.length - counter]; //Creates an array that is the length of the old array minus all the elements skipped
		int j =0;
		
		for (int i=0; i< list.length; i++){ //Loops until every element not equal to target has been stored in new array
			if (target != list[i]) {
				Array2[j] = list[i]; //If target does not equal an element in the list, then store it in the new array
				j++;
			}
		}
		
		if (counter >=1){ //Produces target
			System.out.println("Element " + target + " has been found.");
		} 
		else { //If Counter is equal to zero, then target does not exist in the array
			System.out.println("Element " + target + " was not found. ");
		}
		return Array2; //Returns the new array.
	}	
}

