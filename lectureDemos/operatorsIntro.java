/* Prof AKC
	September 9, 2018
	Review of operators, and discussion of precedence */

public class operatorsIntro{
	public static void main(String[] args){
		/* Swapping two values
		(by the way, this is an example of a 
		block comment) */
		//int swap1 = 1;
		//int swap2 = 2;
		// What's wrong with the following line?
		//swap1 = swap2;
		//swap2 = swap1;
		//System.out.println("This is swap1: " + swap1);
		//System.out.println("This is swap2: " + swap2);
		// How can we fix it?  Use a "holder":
		 //int tempHolder = swap1;
		 //swap1 = swap2;
		 //swap2 = tempholder;
		
		/* Chars are integers! */
		char myChar = 'a';
		char myChar = 99;
		System.out.println("This is the char a: " + myChar);
		myChar = myChar++;
		System.out.println("This is the char b: " + myChar++);
		
		/* Since we haven't done much with booleans, let use one
		for a precedence question */ 
		//boolean myBool = 5 < 2 + (double) 7 && 4 + 6 >= 2;
		//System.out.println("The value of myBool is " + myBool);
		
		/* Pre/post Increment */
		//int myCounter = 1;
		//myCounter = myCounter + 1;
		//System.out.println("The value of myCounter is now: " + myCounter);
		//System.out.println("Using preincrement, the value of myCounter becomes: " + ++myCounter);
		//System.out.println("Using postincrement, the value of myCounter becomes: " + myCounter++);
		// Can you write code that analogously demonstrates how pre/post decrement work?
		
		/* Augmented Assignment Operator */
		//int addTwo = 2;
		//addTwo = addTwo + 2;
		//System.out.println("This is the value of addTwo: " + addTwo);
		//addTwo += 2;
		//System.out.println("This is the value of addTwo using the augmented operator: " + addTwo);
		// Can you write code that analogously demonstrates subtraction, multiplication, and division using
		//  the correct augmented operator for each?
		
		/* Another precedence example */
		//double val1; int val2;
		//val1 = 2.3;
		//val2 = 9;
		//int val3 = (int) val1 * val2 - (int) val1;
		//System.out.println("val 3 = " + val3);
		
	}
}
