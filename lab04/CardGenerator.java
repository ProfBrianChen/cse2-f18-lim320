/* Larrisa Miller
  9/21/2018
  CSE 02
  Card Generator: program picks a random card from a deck. 
  */

public class CardGenerator{
  public static void main (String [] args){
    
    // select number for card
    int cardSuit = (int)(Math.random()*(52+1))+1; //randomizes the number of the suit of cards
    int cardNumber = (int)(Math.random()*(13+1))+1; // randomizes the card number within the suit selected above
    
    // create variables to hold card names
    String cardSuitName = new String(); 
    String cardNumberName = new String();
    
    // turns random number into suit 
    if (cardSuit <= 13){ // if the random number generated for card suit is 1-13, the card will have the name "Diamonds"
       cardSuitName = "Diamonds";
    }
    else if (cardSuit >= 14 && cardSuit <= 26){
      cardSuitName = "Clubs";
    }
    else if (cardSuit >= 27 && cardSuit <= 39){
      cardSuitName = "Hearts";
    }
    else {
      cardSuitName = "Spades";
    }
    
    
   // turns random number into card name 
   switch (cardNumber){
     case 1: cardNumberName = "Ace"; //if the random number generated for cardNumber is 1, it will be assigned the name "Ace"
      break;
     case 2: cardNumberName = "2";
      break;
     case 3: cardNumberName = "3";
       break;
     case 4: cardNumberName = "4";
       break;
     case 5: cardNumberName = "5";
       break;
     case 6: cardNumberName = "6";
       break;
     case 7: cardNumberName = "7";
       break;
     case 8: cardNumberName = "8";
       break;
     case 9: cardNumberName = "9";
       break;
     case 10: cardNumberName = "10";
       break;
     case 11: cardNumberName = "Jack";
       break;
     case 12: cardNumberName = "Queen";
       break;
     case 13: cardNumberName = "King";
       break;
  }
    System.out.println("You picked the " + cardNumberName + " of " + cardSuitName); //informs the user what card they have drawn
}
}