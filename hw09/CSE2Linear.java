import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
	static int binaryCounter = 0;//Sets a counter at 0 for all methods
	
	public static void main (String [] args) {
		Scanner myScanner = new Scanner(System.in); 
		int [] finalGrades = new int [15]; // create array to hold 15 final grades. 
		System.out.println("Enter 15 ascending ints for final grades in CSE2:"); // prompts user to enter integers
		
		for (int i = 0; i < finalGrades.length; i++) { // loops through 
		// ensuring user enters an int
		boolean isIntBool = myScanner.hasNextInt(); // scanner tests if what user enters is an int
		while (isIntBool == false) {
			System.out.println("That is not an integer. Please enter an integer value.");
			myScanner.next(); // removes incorrect input to prevent infinite loop
			isIntBool = myScanner.hasNextInt(); // open for new input
		}
		finalGrades[i] = myScanner.nextInt(); // fills in space on array
		
		// ensuring user enters an int from 0-100
		while (finalGrades[i] < 0 || finalGrades[i] > 100) {
			System.out.println("Please enter an integer from 0 - 100.");
			finalGrades[i] = myScanner.nextInt();
		}
		// ensuring user enters ints in ascending order
		if (i > 1) {
			while (finalGrades[i] < finalGrades[i-1]) {
			System.out.println("Please enter all integers in ascending order.");
			myScanner.next(); // removes incorrect number
			finalGrades[i] = myScanner.nextInt();
		}	
	}
		}
		System.out.println(Arrays.toString(finalGrades)); // prints array of grades
		
		System.out.println("Enter a grade to be searched for: "); // asks user to input key
		boolean isKeyBool = myScanner.hasNextInt(); // scanner tests if what user enters is an int
		while (isKeyBool == false) {
			System.out.println("That is not an integer. Please enter an integer value.");
			myScanner.next(); // removes incorrect input to prevent infinite loop
			isKeyBool = myScanner.hasNextInt(); // open for new input
		}
		int key = myScanner.nextInt(); // user's inputted key
		
		int binaryOutput = binarySearch(finalGrades, key);
		if (binaryOutput == -1) {
			System.out.println(key + " was not found in the list with " + binaryCounter + " iteration(s)." );
		}
		else {
			System.out.println(key + " was found in " + binaryCounter + " iteration(s).");
		}
		
		scramble(finalGrades); // calls scramble method
		System.out.println("Scrambled:");
		System.out.println(Arrays.toString(finalGrades)); // prints array of grades
		
		System.out.println("Enter a grade to be searched for: "); // asks user to input key
		boolean isKeyBool2 = myScanner.hasNextInt(); // scanner tests if what user enters is an int
		while (isKeyBool2 == false) {
			System.out.println("That is not an integer. Please enter an integer value.");
			myScanner.next(); // removes incorrect input to prevent infinite loop
			isKeyBool2 = myScanner.hasNextInt(); // open for new input
		}
		int key2 = myScanner.nextInt(); // user's inputted key
		
		int linearOutput = linearSearch(finalGrades, key2);
		if (linearOutput == -1) {
			System.out.println(key2 + " was not found in the list with " + linearOutput + " iteration(s)." );
		}
		else {
			System.out.println(key2 + " was found in " + linearOutput + " iterations(s).");
		}	
	}

	// binary search
	public static int binarySearch (int [] finalGrades, int key) {
		
	
		int low = 0;
		int high = finalGrades.length-1;
		while(high >= low) {
			int mid = (low + high)/2; // divides array into 2 parts
			if (key < finalGrades[mid]) {
				high = mid - 1;
			}
			else if (key == finalGrades[mid]) {
				return key;
			}
			else {
				low = mid + 1;
			}
			binaryCounter++; //Incremement binary Counter
		}
		 
		return -1; 
	}
	
	// scrambles array
	public static void scramble (int [] finalGrades) {
		for (int i = 0; i < finalGrades.length-1; i++) {
			Random rand = new Random();
			int x = rand.nextInt(finalGrades.length-1) + 1; // gives number within array length
			
			int shuffleGrades = finalGrades[0]; // saving array[0]
			finalGrades[0] = finalGrades[x]; 
			finalGrades[x] = shuffleGrades; 
		}
	}
	
	// linear search
	public static int linearSearch (int [] finalGrades, int key2){
		   for (int i =0; i < finalGrades.length; i++){     
		     if (finalGrades[i] == key2){ //Checks if the value of array at i is equal to the input, then returns i +1 if true.
		       return i + 1; //Returns i +1 because array starts at 0
		     }
		   }
		     return -1;//Returns an int out of the bounds if key2 is not found
		 }
	}

