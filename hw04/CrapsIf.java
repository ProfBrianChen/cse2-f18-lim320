/* Larrisa Miller
   9/23/2018
   CSE 002
   CrapsIf: generates slang for 2 die according to the game craps using only if statements
*/
import java.util.Scanner;

public class CrapsIf{
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);
    
    int diceOne = -1; // declaring variables that can be used in all loops; these store the two dice values that will be used for slang.
    int diceTwo = -1; // initialized as -1 to ensure that this value is properly changed within remaining code.
   
   // establishes how user wants to play the game
    System.out.println("If you would like to randomly cast dice, enter 0. If you would like to state the two dice that you want to evaluate, enter 1.");
     int userInput = myScanner.nextInt(); 
    // loops through different options that user can input
     if (userInput == 0){ // if the user wants both dice to be random, this puts a random number from 1-6 in each dice value
       diceOne = (int)(Math.random()*(6+1)) + 1;
       diceTwo = (int)(Math.random()*(6+1)) + 1;
     }
    else if (userInput == 1){ // if user wants to pick out dice values, this gives them the option to input values for each dice
      System.out.println("Please enter the number of your first dice.");
        diceOne = myScanner.nextInt(); // inputs value into dice 1
    
        if (diceOne >= 1 && diceOne <= 6){ 
            System.out.println("Please enter the number of your second dice.");
            diceTwo = myScanner.nextInt();
              if (diceTwo <= 0 || diceTwo > 6){
                System.out.println("Invalid input. Please start the program again and use values between 1 and 6."); // ensures that the user only inputs values 1-6 for dice 2
                return;
              }
          }
        else {
          System.out.println("Invalid input. Please start the program again and use values between 1 and 6"); // ensures that user only puts in values 1-6 for dice 1
          return;
        }
    }
        
   else {
      System.out.println("Invalid input. Please start the program again and use 0 or 1."); // outerloop ensures that user is choosing either random dice or to input two numbers
      return;
    }
    
 // assigns the slang names based on what numbers are generated above
   if (diceOne == 1 && diceTwo == 1) {
     System.out.println("Snake Eyes");
   }
    else if (diceOne == 1 && diceTwo == 2 || diceOne == 2 && diceTwo == 1) {
     System.out.println("Ace Deuce");
   }
    else if (diceOne == 2 && diceTwo == 2) {
      System.out.println("Hard Four");
    }
    else if (diceOne == 3 && diceTwo == 1 || diceOne == 1 && diceTwo == 3){
      System.out.println("Easy Four");
    }
    else if (diceOne + diceTwo == 5) {
      System.out.println("Fever Five");
    }
    else if (diceOne == 3 && diceTwo == 3){
      System.out.println("Hard Six");
  }
    else if (diceOne + diceTwo == 6){
      System.out.println("Easy Six");
    }
    else if (diceOne + diceTwo == 7){
      System.out.println("Seven Out");
    }
    else if (diceOne == 4 && diceTwo == 4){
      System.out.println("Hard Eight");
    }
    else if (diceOne == 3 && diceTwo == 5 || diceOne == 5 && diceTwo == 3){
      System.out.println("Easy Eight");
    }
    else if (diceOne + diceTwo == 9){
      System.out.println("Nine");
    }
    else if (diceOne == 5 && diceTwo == 5){
      System.out.println("Hard Ten");
    }
    else if (diceOne + diceTwo == 10){
      System.out.println("Easy Ten");
    }
    else if (diceOne == 5 && diceTwo == 6 || diceOne == 6 && diceTwo == 5){
      System.out.println("Yo-leven");
    }
    else if (diceOne == 6 && diceTwo == 6){
      System.out.println("Boxcars");
    }
  }
}