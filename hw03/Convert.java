/* Larrisa Miller
   9/16/2018
   CSE 002
   Convert: Convert acres of land and inches of rainfall into cubic miles of rainfall
*/

import java.util.Scanner;

public class Convert {
  public static void main (String [] args){
    Scanner myScanner = new Scanner(System.in);
    
    // prompting user to input acres and inches of rainfall
    System.out.println("Enter the affected area in acres:");
    double acresArea = myScanner.nextDouble(); // creates variable to hold value of acre area
    System.out.println("Enter the rainfall in the affected area:");
    double rainfall = myScanner.nextDouble(); // creates variable to hold value of rainfall in area
    
    // converting entered values into cubic miles of rainfall
    rainfall /= 63360; // change number of inches into miles-- 1 mile = 63360 inches
    acresArea /= 640; // change acres into miles squared-- 1 sq mile = 640 acres 
    double milesRain = rainfall * acresArea; // miles * miles squared of rain = cubic miles of rainfall
    
    //output cubic miles of rainfall
    System.out.println("The quantity of rain in cubic miles is " + milesRain);
  }
}