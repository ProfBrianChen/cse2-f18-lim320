import java.util.Random;
import java.util.Scanner;

public class paragraphGenerator {
public static void main (String [] args) {
	Scanner myScanner = new Scanner(System.in);
	
	 boolean moreSentence;
	do { // loops to give the user option to continue creating sentences
		
		paragraph(); // calls paragraph method to generate sentences for paragraph
		
		System.out.println("Would you like to generate another thesis sentence and paragraph? 1 for yes, 0 for no.");
		// ensuring that user enters proper answer to generate a new sentence or not
		boolean numSentenceBool = myScanner.hasNextInt();// scanner tests if what the user enters is an int
		while (numSentenceBool != true){ // if the user enters something other than an int, the bool is evaulated as false and this loop is taken
			System.out.println("Please enter either 1 or 0."); 
			myScanner.next(); // removes incorrect input to prevent infinite loop
			numSentenceBool = myScanner.hasNextInt(); // open for new input
		}
		int numSentence = myScanner.nextInt();
		if (numSentence == 0) { // if user selects 0, program quits
			moreSentence = false;
			break;
		}
		else if (numSentence == 1) { // if user selects 1, program generates another sentence
			moreSentence = true;
		}
		else {
			System.out.println("Please enter either 0 or 1"); // prompts user to only enter 0 or 1
			numSentence = myScanner.nextInt();
		}
	} while (moreSentence = true); 
} 

public static void paragraph() { //puts sentences together to create paragraph
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(4) + 1; // generates integers less than 4 (1-3 support sentences for the paragraph). +1 ensure that there will be at least one sentence.
	
	// calls methods to create words for thesis sentence
	String adj1 = adjective();
	String adj2 = adjective();
	String subj1 = subject();
	String verb1 = verb();
	String adj3 = adjective();
	String obj1 = object();
	System.out.println(thesis(adj1, adj2, subj1, verb1, adj3, obj1)); // prints original sentence
	
	for (int i = 0; i < randomInt; i++) { // loops to create and print random number of support sentences
		// calls methods to create words for supporting sentences. Creates new words for each sentence.
		String suppAdj1 = supportAdjective();
		String suppAct1 = supportAction();
		String adj4 = adjective();
		String obj2 = object();
		String obj3 = object();
		String suppAct2 = supportAction();
		String obj4 = object();
		
		System.out.println(support(subj1, suppAdj1, suppAct1, adj4, obj2, obj3, suppAct2, obj4));
	}
	
	String suppAct3 = supportAction(); // creates conclusion sentence
	System.out.println(conclusion(subj1, suppAct3)); // prints conclusion sentence
}

public static String thesis(String adj1, String adj2, String subj1, String verb1, String adj3, String obj1) { // generates thesis sentence
	String thesisSentence = ("The " + adj1 + ", " + adj2 + " " + subj1 + " " + verb1 + " the " + adj3 + " " + obj1 + ".");
	return thesisSentence;
}

public static String support(String subj1, String suppAdj1, String suppAct1, String adj4, String obj2, String obj3, String suppAct2, String obj4) { // generates support sentence
	String supportSentence = ("This " + subj1 + " was " + suppAdj1 + " " + suppAct1 + " to the " + adj4 + " " + obj2 + ". It used a " + obj3 + " to be " + suppAct2 + " to the "+ obj4 + "."); // creates second sentence
	return supportSentence;
}

public static String conclusion(String subj1, String suppAct3) { // generates conclusion sentence
	String conclusionSentence = ("That " + subj1 + " was " + suppAct3 + "!");
	return conclusionSentence;
}

// the following methods create individual words
public static String supportAdjective() { // generates adjectives that make sense for support sentences
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String supportAdj = "";
	switch (randomInt) { // assigns a word based on random int
	case 0:
		supportAdj = "particularly";
		break;
	case 1:
		supportAdj = "extremely";
		break;
	case 2:
		supportAdj = "extraordinarily";
		break;
	case 3:
		supportAdj = "quite";
		break;
	case 4:
		supportAdj = "very";
		break;
	case 5:
		supportAdj = "not very";
		break;
	case 6:
		supportAdj = "not at all";
		break;
	case 7:
		supportAdj = "wholly";
		break;
	case 8:
		supportAdj = "unquestionably";
		break;
	case 9:
		supportAdj = "truly";
		break;
	case 10:
	    supportAdj = "positively";
		break;
	}
	return supportAdj;
}

public static String supportAction() { // generates actions that make sense for support sentences
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String suppAct = ""; // declares space to store string
	switch (randomInt) { // assigns a word based on random int
	case 0:
		suppAct = "kind";
		break;
	case 1:
		suppAct = "nice";
		break;
	case 2:
		suppAct = "rude";
		break;
	case 3:
		suppAct = "mean";
		break;
	case 4:
		suppAct = "cold";
		break;
	case 5:
		suppAct = "demeaning";
		break;
	case 6:
		suppAct = "encouraging";
		break;
	case 7:
		suppAct = "sweet";
		break;
	case 8:
		suppAct = "friendly";
		break;
	case 9:
	    suppAct = "unfriendly";
		break;
	case 10:
		suppAct = "cordial";
		break;
	}
	return suppAct;
}

public static String adjective() { // generates an adjective
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String adj = ""; // declares space to store string
	switch (randomInt) { // assigns a word based on random int
	case 0:
		adj = "small";
		break;
	case 1:
		adj = "large";
		break;
	case 2:
		adj = "short";
		break;
	case 3:
		adj = "tall";
		break;
	case 4:
		adj = "quiet";
		break;
	case 5:
		adj = "loud";
		break;
	case 6:
		adj = "energetic";
		break;
	case 7:
		adj = "tired";
		break;
	case 8:
		adj = "friendly";
		break;
	case 9:
		adj = "shy";
		break;
	case 10:
		adj = "confused";
		break;
	}
	return adj;
}
public static String subject() { // generates a subj
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String subj = ""; // declares space to store string
	switch (randomInt) { // assigns a word based on random int
	case 0:
		subj = "dog";
		break;
	case 1:
		subj = "cat";
		break;
	case 2:
		subj = "hedgehog";
		break;
	case 3:
		subj = "fox";
		break;
	case 4:
		subj = "penguin";
		break;
	case 5:
		subj = "goat";
		break;
	case 6:
		subj = "rabbit";
		break;
	case 7:
		subj = "hamster";
		break;
	case 8:
		subj = "chicken";
		break;
	case 9:
		subj = "horse";
		break;
	case 10:
		subj = "turtle";
		break;
	}
	return subj;
}
public static String verb() { // generates a past-tense verb
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String verb = ""; // declares space to store string
	switch (randomInt) { // assigns a word based on random int
	case 0:
		verb = "passed";
		break;
	case 1:
		verb = "looked at";
		break;
	case 2:
		verb = "walked by";
		break;
	case 3:
		verb = "ran past";
		break;
	case 4:
		 verb = "smiled at";
		break;
	case 5:
		verb = "leaped towards";
		break;
	case 6:
		verb = "avoided";
		break;
	case 7:
		verb = "hid from";
		break;
	case 8:
		verb = "went near";
		break;
	case 9:
		verb = "stared at";
		break;
	case 10:
		verb = "yelled at";
		break;
	}
	return verb;
}

public static String object() { // generates an object
	Random randomGenerator = new Random(); // create random number generator
	int randomInt = randomGenerator.nextInt(10); // generates integers less than 10
	String obj = ""; // declares space to store string
	switch (randomInt) { // assigns a word based on random int
	case 0:
		obj = "succulent";
		break;
	case 1:
		obj = "pen";
		break;
	case 2:
		obj = "laptop";
		break;
	case 3:
		obj = "lamp";
		break;
	case 4:
		obj = "window";
		break;
	case 5:
		obj = "desk";
		break;
	case 6:
		obj = "speaker";
		break;
	case 7:
		obj = "candle";
		break;
	case 8:
		obj = "chair";
		break;
	case 9:
		obj = "building";
		break;
	case 10:
		obj = "college student";
		break;
	}
	return obj;
}
}

