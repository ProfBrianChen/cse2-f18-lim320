import java.lang.reflect.Array;
import java.lang.Math;
import java.util.Scanner;

public class Shuffling {
	public static void main(String[] args) {  // main method provided in HW
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 
			System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		while(again == 1){ 
			hand = getHand(cards,index,numCards); 
			printArray(hand);
			index = index - numCards;
			System.out.println("Enter a 1 if you want another hand drawn"); 
			again = scan.nextInt(); 
		}  
	} 

	// prints array
	public static void printArray(String [] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");	
		}
		System.out.println();
	}
	// shuffles array
	public static void shuffle(String [] array) {
		for (int i = 0; i < 52; i++) { // swaps 52 times
			int x = (int)(Math.random()*51) + 1; // gives number between 1 and 51 because array is 0 - 51
			String array0 = array[0]; // saving array[0]
			array[0] = array[x]; 
			array[x] = array0; 
		}
	}
	public static String [] getHand(String [] array, int index, int numCards) {
		if (numCards >= index + 1) {  // creating new deck by shuffling the old one and resetting index to end
			shuffle(array);
			index = 51;
		}
		String [] hand = new String [numCards]; // creates string that holds amount numCards specifies
		int counter = 0;
		for (int i = index; i > index - numCards; i--) {
			hand[counter] = array[i];
			counter++;
		}
		return hand;
	}
}
