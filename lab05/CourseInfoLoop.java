	/* Larrisa Miller
		  10/05/2018
		  CSE 02
		  Course Info Loop: asks user for information about a course and won't move on until satisfactory information is entered
		  */
import java.util.Scanner;
public class CourseInfoLoop {

	public static void main(String[] args) {
	
		    Scanner myScanner = new Scanner(System.in);
		   
		    // acquiring course number
		    boolean courseNumBool; // creates boolean value for course number to be tested later
		    System.out.println("Please enter the course number.");
		    courseNumBool = myScanner.hasNextInt();// scanner tests if what the user enters is an int
		    while (courseNumBool != true){ // if the user enters something other than an int, the bool is evaulated as false and this loop is taken
		      System.out.println("That is not a number. Please enter a course number, such as 101 or 001."); 
		      myScanner.next(); // removes incorrect input to prevent infinite loop
		      courseNumBool = myScanner.hasNextInt(); // open for new input
		    }
		    
		    myScanner.next();
		    
		    
		    // acquiring dept name
		     boolean deptNameBool; // creates boolean value for dept name
		    System.out.println("Please enter the department name.");
		    deptNameBool = myScanner.hasNext(); // scanner tests if what the user enters is a string
		    while (deptNameBool != true){ // if the user enters something other than a string, the bool is evaluated as false
		      System.out.println("That is not a name. Please enter a string value, such as Computer Science.");
		      myScanner.next(); // removes incorrect input to prevent infinite loop
		     deptNameBool = myScanner.hasNext(); 
		    } 
		    myScanner.next();
		    
		    // acquiring number of times a class meets in a week
		    boolean numMeetBool; // creates boolean value to be tested later
		    System.out.println("Please enter how many times the class meets in a week.");
		    numMeetBool = myScanner.hasNextInt();
		    while (numMeetBool != true){
		      System.out.println("That is not a number. Please enter an integer, such as 2 or 3.");
		      myScanner.next();
		      numMeetBool = myScanner.hasNextInt();
		    }
		    myScanner.next();
		    
		    // acquiring what time the class starts
		    boolean classTimeBool; // creates boolean value for time class starts
		    System.out.println("Please enter the time that the class starts in the form 1200.");
		    classTimeBool = myScanner.hasNextInt();
		    while (classTimeBool != true){
		      System.out.println("That is not a number. Please enter a time in the form 1200.");
		      myScanner.next();
		      classTimeBool = myScanner.hasNextInt();
		    }
		    myScanner.next();
		   
		    // acquiring professor name
		     boolean profNameBool; // creates boolean value for professor's name
		    System.out.println("Please enter the department name.");
		    profNameBool = myScanner.hasNext(); // scanner tests if what the user enters is a string
		    while (profNameBool != true){ // if the user enters something other than a string, the bool is evaluated as false
		      System.out.println("That is not a name. Please enter a name.");
		      myScanner.next(); // removes incorrect input to prevent infinite loop
		     profNameBool = myScanner.hasNext(); 
		    } 
		    myScanner.next();
		    
		    // acquiring number of students in the class
		    boolean numStudents; // creates boolean value to be tested later
		    System.out.println("Please enter how many times the class meets in a week.");
		    numStudents = myScanner.hasNextInt();
		    while (numStudents != true){
		      System.out.println("That is not a number. Please enter an integer, such as 2 or 3.");
		      myScanner.next();
		      numStudents = myScanner.hasNextInt();
		    }
		    myScanner.next();
		  }
			

	}
