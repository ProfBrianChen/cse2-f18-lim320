/* Larrisa Miller
   9/17/2018
   CSE 002
   Pyramid: takes dimensions of pyramid and computes the volume
*/

import java.util.Scanner;

public class Pyramid{
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);
    
    // prompting user to input pyramid dimensions
    System.out.println("Enter the square side length of the pyramid: ");
    double pyramidLength = myScanner.nextDouble();
    System.out.println("Enter the height of the pyramid: ");
    double pyramidHeight = myScanner.nextDouble();
    
    // calculating pyramid volume
    double pyramidVolume = pyramidLength * pyramidLength * pyramidHeight / 3; // the formula for pyramid volume is length * width * height / 3
    
    // output volume of pyramid
    System.out.println("The volume of the pyramid is " + pyramidVolume + " units.");
    
  }
}