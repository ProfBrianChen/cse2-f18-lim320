import java.util.Scanner;

public class methodDemo{
	public static void main(String[] arg){
		final int MYINT1 = 2;
		final int MYINT2 = 3;
		final double MYDOUBLE1 = -3.14;
		final double MYDOUBLE2 = 1.5708; // ~pi/2
		final double MYDOUBLE3 = 2.9;
		final String MYSTRING = "the output is ";
		// What happens when we try to change a constant?
		//MYINT1 = 4;
		
		int power = (int) Math.pow(MYINT1,MYINT2); // We have to explicitly cast
		                                           // to an integer here! Why?
		System.out.println("For 2^3, " + MYSTRING + power + ".");
		
		int ceil = (int) Math.ceil(MYDOUBLE3); // We explicitly cast again!
		System.out.println("For ceil(" + MYDOUBLE3 + "), " + MYSTRING + ceil + ".");
		
		double nextUp = Math.nextUp(MYDOUBLE2); // Why don't we cast here?
		System.out.println("When rounding " + MYDOUBLE2 + MYSTRING + nextUp + ".");
		
		double abs = Math.abs(MYDOUBLE1);
		System.out.println("When calculating abs("  + MYDOUBLE1 + "), " + MYSTRING + abs + ".");
		
		double sin = Math.sin(MYDOUBLE2);
		System.out.println("When calculating sin("  + MYDOUBLE2 + "), " + MYSTRING + sin + ".");
		
	}
	
	
	
}
