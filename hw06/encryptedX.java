/* Larrisa Miller
 * Due 10/23/2018
 * CSE 002 Hw 06
 * encryptedX: creates a hidden x in number of rows user specifies
 */
import java.util.Scanner;
public class encryptedX {
public static void main (String [] args) {
	Scanner myScanner = new Scanner(System.in);
	
	// acquiring size of square
	System.out.println("Please enter an integer between 1 and 100.");
	boolean userInputBool = myScanner.hasNextInt();
	while (userInputBool != true) { // ensures that user enters an integer
		System.out.println("That is not an integer value. Please enter an integer value.");
		myScanner.next(); // removes incorrect input to prevent infinite loop
		userInputBool = myScanner.hasNextInt(); // open for new input
	}
	int userInput = myScanner.nextInt(); 
	while (userInput < 1 || userInput > 100) { // ensures that the number is in the right range of values
		System.out.println("Please enter a number between 1 and 100.");
		userInput = myScanner.nextInt();
	}
	for (int numRows = 1; numRows <= userInput; numRows++) { // creates the box
		for (int numLine = 1; numLine <= userInput; numLine++) {
			if (numRows == numLine) { // creates the diagonal of the x from left to right
				System.out.print(" ");
			}
			else if (numLine == userInput - numRows) { // creates the diagonal of the x from right to left
				System.out.print(" ");
			}
			else {
				System.out.print("*");
			}
		}
		System.out.println("");
	}
}
}



