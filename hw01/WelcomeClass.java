///////////////
//// CSE 02 Welcome Class
///

public class WelcomeClass{

  public static void main(String args[]){
  /// prints following welcome message and ID to terminal
  System.out.println("  -----------");
  System.out.println("  | WELCOME |");
  System.out.println("  -----------");
  System.out.println("  ^  ^  ^  ^  ^  ^");
  System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("<-L--I--M--3--2--0->");
  System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("  v  v  v  v  v  v");
  
  /// prints out autobiographic statement to terminal
  System.out.print("I study Cognitive Science and Psychology; if time permits, I also want a minor in Sociology & Anthropology. ");
  System.out.print("I am in the Marching 97 drumline, Kappa Kappa Psi, and I do research for a variety of labs on campus.");
  
  }
}