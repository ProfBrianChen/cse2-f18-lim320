import java.util.Scanner;
public class wordTools {
   public static void main (String [] args) {
       String userInput = sampleText(); // call method for user input
       printMenu(userInput); // call method for user to select from menu
   }

   // accepts user's sample text
   public static String sampleText() {
       Scanner myScanner = new Scanner(System.in);
       System.out.println("Enter a sample text:");
       String userInput = myScanner.nextLine();
       System.out.println("You entered: " + userInput);
       return userInput; // stores the user's input
   }

   // outputs menu to user and allows them to choose what to do
   public static void printMenu(String userInput) {
       Scanner myScanner = new Scanner(System.in);
       System.out.println("Enter the letter associated with the menu option you would like to use.");
       System.out.println("MENU");
       System.out.println("c - number of non-whitespace characters");
       System.out.println("w - number of words");
       System.out.println("f - find text");
       System.out.println("r - replace all !s");
       System.out.println("s - shorten spaces");
       System.out.println("q - Quit");

       char userMenuChoice = myScanner.next().charAt(0); //turns the string user enters into a char
       // loops through the user's options
       if (userMenuChoice == 'q') {
           System.exit(0); // quits program
       }
       else if (userMenuChoice == 'c') {
           int num = getNumOfNonWSCharacters(userInput); // calls getNumofNonWSCharacters method
           System.out.println(num);
       }
       else if (userMenuChoice == 'w') {
           int num = getNumOfWords(userInput); // calls getNumOfWords
           System.out.println(num);
       }
       else if (userMenuChoice == 'f') {
           Scanner myScanner2 = new Scanner(System.in);
           System.out.println("Enter a word or phrase to be found");
           String text = myScanner2.next(); // accepts word user wants found
           int num = findText(text, userInput); // plugs word user wants found into method
           System.out.println(num);
       }
       else if (userMenuChoice == 'r') {
           String newText=replaceExclamation(userInput); // calls replaceExplanation method
           System.out.println(newText);
       }
       else if (userMenuChoice == 's') {
           String newText=shortenSpace(userInput); // calls shortenSpace method
           System.out.println(newText);
       }
       else { // makes sure that user enters a menu option
           System.out.println("That is not a menu option. Please enter a character listed on the menu.");
       }
   }

   public static int getNumOfNonWSCharacters(String userInput) {
       int count = 0;
       int numNonWS = 0;
       for(int i = 0; i < userInput.length(); i++) {
           if(Character.isWhitespace(userInput.charAt(i))) // determines number of white space
               count++;
       }
       numNonWS = userInput.length() - count; // subtracts white space from total characters to get non whitespace
       return numNonWS;
   }

   public static int getNumOfWords(String userInput) {
       int count = 0;
       int numWords = 0;
       for(int i = 0; i < userInput.length(); i++) {
           if(Character.isWhitespace(userInput.charAt(i))) // whitespace signifies a new word
               count++;
       }
       numWords = count + 1; // adding one includes the first word which does not have a white space in front of it
       return numWords;
   }

   public static int findText(String text, String userInput){
       int textLength = text.length(); // string user wants found
       int inputLength = userInput.length(); // original userInput
       int count=0;
       for(int i = 0; i<inputLength; i++) { // loops through original input
           if (userInput.charAt(i) == text.charAt(0)) {
               for (int j = 1; j < textLength && i+j < inputLength; j++) { // compares text to user input
                   if (userInput.charAt(i+j) != text.charAt(j)) {
                       break;
                   }
                   if (j==textLength-1){
                       count++;
                   }
               }
           }
       }
       return count;
   }
  
   public static String replaceExclamation(String userInput) {
       String text = " "; // intiialize words
       char letters; // initialize letters in string
       for(int i =0; i<userInput.length(); i++) {
           if(userInput.charAt(i) != '!') { // only change characters if !
               letters = userInput.charAt(i);
               text += letters; // return word as was
           }
           else {
               letters = '.'; // if ! change to .
               text += letters; // add changed . onto words
           }
       }
       return text; // returns edited text

   }

   public static String shortenSpace(String userInput) {
       while (userInput.indexOf("  ") != -1){ // will only run if double space occurs
           userInput = userInput.replace("  ", " "); // replaces double space with single space
       }
       return userInput;
   }
}


